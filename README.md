# Demo Assignment for Travo

This is a demo assignment for [Travo](https://gitlab.com/travo-cr/travo).

To try the instructions below, you will need:
- [travo](https://gitlab.com/travo-cr/travo) installed
- an account on [GitLab.com](https://Gitlab.com) (just because the
  assignment is hosted there).

## Instructions

1.  Fetch the assignment with:

        travo fetch https://gitlab.com/travo-cr/demo-assignment.git

2.  Edit the content of the assignment in the directory
    `demo-assignment/` as desired; for example, open the README.md
    file, and answer the questions at the end of it.

3.  Submit the assignment with:

        travo submit demo-assignment

You may fetch and submit as many times as desired.

You can browse your submission online through the URL that is
displayed upon submission.

You can remove your submission with:

    travo remove_submission  # not yet implemented

## The assignment

What is your favorite color?

[type your answer here]
